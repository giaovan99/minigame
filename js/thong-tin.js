let formThongTin = document.querySelector("form#thongTinCaNhan"); //Form thông tin
let sectionThongTin = document.querySelector("#sectionThongTin"); //Section thông tin

formThongTin &&
  formThongTin.addEventListener("submit", (event) => {
    event.preventDefault();
    let data = getFormData(event);
    // Xử lý api mã đăng nhập
    console.log(data);
    // Ẩn thông tin, chuyển sang chọn quà
    if (data) {
      window.location.assign("/ket-qua.html");
    }
  });
