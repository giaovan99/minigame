const presents = document.querySelectorAll(".present");
const displayCode = document.querySelector(".displayCode");

// Ẩn 2 hộp quà không được chọn
function hidePresents(i) {
  presents.forEach((present, index) => {
    if (index !== i) {
      present.parentNode.classList.add("hidden");
      setTimeout(() => present.parentNode.remove(), 2000);
    } else {
      setTimeout(() => {
        present.parentNode.classList.remove("col-4");
        present.parentNode.classList.add("active");
      }, 2000);
    }
  });
}

//

// Sự kiện mở hộp quà
presents.forEach((present, index) => {
  present.addEventListener("click", () => {
    // Ẩn 2 hộp quà khác đi
    hidePresents(index);
    // Mở hộp quà
    setTimeout(() => {
      // Mở hộp quà + pháo hoa
      let pyro = document.querySelector(".pyro");
      pyro.classList.remove("hidden");
      present.classList.toggle("open");
      // Sau 3s tắt pháo hoa
      setTimeout(() => {
        pyro.classList.add("hidden");
      }, 5000);
      // Gắn giải thưởng & scale lên
      present.querySelector(".name").innerHTML = `<img
      src="./img/phan-thuong/phuong-thuong-1.png"
      width="110"
      height="110"
    />`;
      // ẩn hộp đi
      present.querySelector(".rotate-container").classList.add("hidden");
    }, 2200);
    setTimeout(() => {
      // Hiển thị title kết quả
      titleChonQua.innerHTML = `<img src="./img/ket-qua-boc-tham.png" alt="" width="100%" />`;
      // Hiển thị mã dự thưởng
      displayCode.classList.remove("hidden");
    }, 2150);
  });
});
