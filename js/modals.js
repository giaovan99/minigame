let formTraCuu = document.querySelector("form#traCuu"); //nút tra cứu mã dự thưởng
let traCuuModal = document.getElementById("traCuuModal"); //Modal tra cứu

// --------------------------------------Lấy dữ liệu từ form--------------------------------------
function getFormData(event) {
  const formData = new FormData(event.target);
  const formProps = Object.fromEntries(formData);
  return formProps;
}

// --------------------------------------Xử lý tra cứu mã dự thưởng--------------------------------------
formTraCuu &&
  formTraCuu.addEventListener("submit", (event) => {
    event.preventDefault();
    let data = getFormData(event);
    // Xử lý api mã dự thưởng
    console.log(data);
    //   Trả về kết quả
    formTraCuu.querySelector(
      "#ketQuaTraCuu"
    ).innerHTML = `<div class="d-flex flex-column gap-1">
  <p class="text-danger">Phần Thưởng: Điện Thoại Oppo</p>
  <p>Ngày Tham Dự: 02/02/2024</p>
  </div>`;
  });

// --------------------------------------Reset form Tra cứu khi Modal đóng--------------------------------------
traCuuModal &&
  traCuuModal.addEventListener("hidden.bs.modal", (event) => {
    traCuuModal.querySelector("form").reset();
    traCuuModal.querySelector(".formWarning").innerHTML = "";
  });
